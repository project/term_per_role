Readme file for the Term per role module for Drupal
---------------------------------------------

Installation:
  Installation is like with all normal drupal modules:
  extract the 'term_per_role' folder from the tar ball to the
  modules directory from your website (typically sites/all/modules).

Dependencies:
  Term per role has taxonomy module in dependence.

Configuration:
  Just open to edit edit or create term and add seetings to new fieldset 
  that allows you to restrict access by role or close term page for all roles.
  In admin area(path is admin/config/content/term-per-role) you can change
  behavior if access is denied to page(show page 404 or 403).

Author
------
Dmitry Drozdik
dmitry.drozdik@gmail.com